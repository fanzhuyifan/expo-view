import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window

import org.kde.kwin

Window {
    id: root
    width: 600
    height: 400
    visible: true

    property int tileSerial: 0

    ColumnLayout {
        anchors.fill: parent

        ExpoView {
            id: view
            Layout.fillWidth: true
            Layout.fillHeight: true

            organized: false

            model: ListModel {
                ListElement {
                    x: 0
                    y: 0
                    width: 50
                    height: 50
                    color: "red"
                }
                ListElement {
                    x: 25
                    y: 25
                    width: 100
                    height: 100
                    color: "blue"
                }
            }

            delegate: ExpoViewDelegate {
                id: delegate
                naturalX: model.x
                naturalY: model.y
                naturalWidth: model.width
                naturalHeight: model.height

                Rectangle {
                    anchors.fill: parent
                    color: model.color
                }

                Text {
                    anchors.centerIn: parent
                    Component.onCompleted: text = root.tileSerial++;
                }

                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    onClicked: event => {
                        switch (event.button) {
                        case Qt.LeftButton:
                            view.model.remove(index);
                            break;
                        case Qt.RightButton:
                            delegate.enabled = !delegate.enabled;
                            break;
                        }
                    }
                }
            }

            displaced: Transition {
                NumberAnimation {
                    properties: "x,y,width,height"
                    duration: 250
                    easing.type: Easing.InOutCubic
                }
            }
        }

        RowLayout {
            Button {
                text: "Add"
                onClicked: {
                    const randomInt = max => Math.floor(Math.random() * max);
                    const randomRange = (min, max) => (min + Math.floor(Math.random() * (max - min)));
                    const colors = ["cyan", "red", "green", "blue"];
                    view.model.append({
                        x: randomRange(view.width * 0.25, view.width * 0.5),
                        y: randomRange(view.height * 0.25, view.height * 0.5),
                        width: randomRange(view.width * 0.25, view.width * 0.5),
                        height: randomRange(view.height * 0.25, view.height * 0.5),
                        color: colors[randomInt(colors.length)],
                    });
                }
            }

            CheckBox {
                checked: view.organized
                onToggled: view.organized = !view.organized
            }

            Text {
                text: "Organized"
            }

            CheckBox {
                checked: view.fillGaps
                onToggled: view.fillGaps = !view.fillGaps
            }

            Text {
                text: "Fill gaps"
            }

            // List selector to set LayoutMode
            ListModel {
                id: layoutModes
                ListElement { name: "Closest" }
                ListElement { name: "Natural" }
                ListElement { name: "Smart" }
            }

            ComboBox {
                model: layoutModes
                currentIndex: view.mode
                onCurrentIndexChanged: view.mode = currentIndex
            }
        }
    }
}
