## ExpoView

#### Building and Running

```
mkdir build
cd build
cmake ..
cmake --build .
bin/expoview
```


#### Example usage

```qml
ExpoView {
    id: view
    organized: true

    model: ListModel {
        ListElement {
            x: 0; y: 0
            width: 50; height: 50
            color: "red"
        }
        ListElement {
            x: 25; y: 25
            width: 100; height: 100
            color: "blue"
        }
    }

    delegate: ExpoViewDelegate {
        id: delegate
        naturalX: model.x
        naturalY: model.y
        naturalWidth: model.width
        naturalHeight: model.height

        Rectangle {
            anchors.fill: parent
            color: model.color
        }
    }

    displaced: Transition {
        NumberAnimation {
            properties: "x,y,width,height"
            duration: 250
            easing.type: Easing.InOutCubic
        }
    }
}
```

![unorganized](doc/unorganized.png)

![organized](doc/organized.png)
