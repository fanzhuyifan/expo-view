#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "expoview.h"

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<ExpoView>("org.kde.kwin", 3, 0, "ExpoView");
    qmlRegisterType<ExpoViewDelegate>("org.kde.kwin", 3, 0, "ExpoViewDelegate");

    engine.load(QUrl("qrc:/main.qml"));

    return app.exec();
}
